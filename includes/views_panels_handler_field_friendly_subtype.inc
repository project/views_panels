<?php

/**
 * @file
 * Definition of panels_views_handler_field_friendly_subtype.
 */

/**
 * Field handler which shows a human-readable pane type/subtype.
 * @ingroup views_field_handlers
 */
class views_panels_handler_field_friendly_subtype extends views_handler_field {

  function query($use_groupby = FALSE) {
    parent::query($use_groupby);

    $this->type_field_alias = $this->query->add_field($this->table_alias, 'type');
  }

  function render($values) {
    $type = $values->{$this->type_field_alias};
    $subtype = $values->{$this->field_alias};

    // For FPPs, use the bundle label, rather than the entity label.
    if ($type == 'fieldable_panels_pane' && function_exists('fieldable_panels_panes_load_from_subtype') && ($fpp = fieldable_panels_panes_load_from_subtype($subtype))) {
      $fpp_info = entity_get_info('fieldable_panels_pane');
      return !empty($fpp_info['bundles'][$fpp->bundle]['label']) ? check_plain($fpp_info['bundles'][$fpp->bundle]['label']) : $fpp->bundle;
    }

    // For most CTools content types, we'll be able to lookup via CTools.
    ctools_include('content');
    $info = ctools_content_get_subtype($type, $subtype);
    if (!empty($info['title'])) {
      return check_plain($info['title']);
    }

    // But if everything fails, just output the type and subtype.
    return "{$type}:{$subtype}";
  }
}
