<?php

/**
 * @file
 * Definition of views_panels_handler_relationship_panelizer_entity.
 */

/**
 * A relationship handlers which join to Panelizer entities.
 *
 * @ingroup views_relationship_handlers
 */
class views_panels_handler_relationship_panelizer_entity extends views_handler_relationship  {

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    $this->ensure_my_table();

    if (!empty($this->definition['join_handler']) && class_exists($this->definition['join_handler'])) {
      $join_handler = $this->definition['join_handler'];
    }
    else {
      $join_handler = 'views_join';
    }

    // First, connect our table to 'panelizer_entity'.
    $first = array(
      'left_table' => $this->table_alias,
      'left_field' => $this->real_field,
      'table' => 'panelizer_entity',
      'field' => 'did',
      'extra' => array(
        array('field' => 'entity_type', 'value' => $this->definition['entity_type']),
      ),
    );
    if (!empty($this->options['required'])) {
      $first['type'] = 'INNER';
    }
    $first_join = new $join_handler();
    $first_join->definition = $first;
    $first_join->construct();
    $first_join->adjusted = TRUE;

    $this->first_alias = $this->query->add_table('panelizer_entity', $this->relationship, $first_join);

    $entity_info = entity_get_info($this->definition['entity_type']);
    $panelizer_entity_field = !empty($this->definition['entity field']) ? $this->definition['entity field'] :
      (!empty($entity_info['revision table']) ? 'revision_id' : 'entity_id');

    if (!empty($this->definition['base field'])) {
      $base_field = $this->definition['base field'];
    }
    else {
      // Figure out what field to join on.
      if ($panelizer_entity_field == 'entity_id') {
        $base_field = $entity_info['entity keys']['id'];
      }
      else {
        $base_field = $entity_info['entity keys']['revision'];
      }
    }

    // Second, we relate to the base table.
    $second = array(
      'left_table' => $this->first_alias,
      'left_field' => $panelizer_entity_field,
      'table' => $this->definition['base'],
      'field' => $base_field,
    );
    if (!empty($this->options['required'])) {
      $second['type'] = 'INNER';
    }
    $second_join = new $join_handler();
    $second_join->definition = $second;
    $second_join->construct();
    $second_join->adjusted = TRUE;

    $alias = 'panelizer_entity_' . $this->definition['entity_type'];
    $this->alias = $this->query->add_relationship($alias, $second_join, $this->definition['base'], $this->relationship);
  }

}
