<?php

/**
 * @file
 * Provide views data and handlers for Fieldable Panels Panes.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function views_panels_fpp_views_data() {
  $data = array();

  $data['views_panels_fpp_usage']['table']['group'] = t('Panels pane');

  $data['views_panels_fpp_usage']['table']['join'] = array(
    'panels_pane' => array(
      'left_field' => 'pid',
      'field' => 'pid',
    ),
  );

  $data['views_panels_fpp_usage']['fpid'] = array(
    'title' => t('Fieldable Panels Pane'),
    'help' => t('The associated Fieldable Panels Pane.'),
    'relationship' => array(
      'field' => 'fpp',
      'handler' => 'views_handler_relationship',
      'base' => 'fieldable_panels_panes',
      'entity_type' => 'fieldable_panels_pane',
      'relationship field' => 'fpid',
    ),
  );

  return $data;
}

