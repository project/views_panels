<?php

/**
 * @file
 * Provide views data and handlers for Panels tables.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function views_panels_views_data() {
  $data = array();

  //
  // The 'panels_pane' table.
  //

  $data['panels_pane']['table']['group'] = t('Panels pane');

  $data['panels_pane']['table']['base'] = array(
    'field' => 'pid',
    'title' => t('Panels panes'),
    'defaults' => array(
      'field' => 'type',
    ),
  );

  $data['panels_pane']['pid'] = array(
    'title' => t('Pid'),
    'help' => t('The Panels pane ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_pane']['did'] = array(
    'title' => t('Did'),
    'help' => t('The Panels display ID this pane is on.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_pane']['panel'] = array(
    'title' => t('Region'),
    'help' => t('The Panels pane region.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_pane']['type'] = array(
    'title' => t('Type'),
    'help' => t('The Panels pane type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_pane']['subtype'] = array(
    'title' => t('Subtype'),
    'help' => t('The Panels pane subtype.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_pane']['friendly_subtype'] = array(
    'title' => t('Friendly type/sybtype'),
    'help' => t('Human-readable version of the type and subtype'),
    'real field' => 'subtype',
    'field' => array(
      'handler' => 'views_panels_handler_field_friendly_subtype',
    ),
  );


  $data['panels_pane']['shown'] = array(
    'title' => t('Shown'),
    'help' => t('Whether or not the pane is shown.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'shown-notshown' => array(t('Shown'), t('Not shown')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Shown'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_pane']['access'] = array(
    'title' => t('Access'),
    'help' => t('Panels pane access information.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_pane']['configuration'] = array(
    'title' => t('Configuration'),
    'help' => t('Panels pane configuration.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_pane']['cache'] = array(
    'title' => t('Cache'),
    'help' => t('Panels pane cache information.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_pane']['style'] = array(
    'title' => t('Style'),
    'help' => t('Panels pane style information.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_pane']['css'] = array(
    'title' => t('CSS'),
    'help' => t('Panels pane CSS information.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_pane']['extras'] = array(
    'title' => t('Extras'),
    'help' => t('Panels pane extra information.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_pane']['position'] = array(
    'title' => t('Position'),
    'help' => t('The Panels pane position in the region.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_pane']['locks'] = array(
    'title' => t('Locks'),
    'help' => t('Panels pane lock information.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_pane']['uuid'] = array(
    'title' => t('UUID'),
    'help' => t('The Panels pane UUID.'),
    'field' => array(
      'handler' => 'views_handler_field',
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  //
  // The 'panels_display' table.
  //

  $data['panels_display']['table']['group'] = t('Panels display');

  $data['panels_display']['table']['base'] = array(
    'field' => 'did',
    'title' => t('Panels displays'),
    'defaults' => array(
      'field' => 'title',
    ),
  );

  $data['panels_display']['table']['join'] = array(
    'panels_pane' => array(
      'left_field' => 'did',
      'field' => 'did',
    ),
  );

  $data['panels_display']['did'] = array(
    'title' => t('Did'),
    'help' => t('The Panels display ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_display']['layout'] = array(
    'title' => t('Layout'),
    'help' => t('The Panels layout.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_display']['layout_settings'] = array(
    'title' => t('Layout settings'),
    'help' => t('The Panels layout settings.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_display']['panel_settings'] = array(
    'title' => t('Panel settings'),
    'help' => t('The Panels settings.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_display']['cache'] = array(
    'title' => t('Panel cache'),
    'help' => t('The Panels cache information.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
    ),
  );

  $data['panels_display']['title'] = array(
    'title' => t('Title'),
    'help' => t('The Panels title.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_display']['hide_title'] = array(
    'title' => t('Hide title'),
    'help' => t('Whether or not the Panels title is hidden.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'hidden-nothidden' => array(t('Hidden'), t('Not hidden')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Shown'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // @todo add title_pane column

  $data['panels_display']['uuid'] = array(
    'title' => t('UUID'),
    'help' => t('The Panels UUID.'),
    'field' => array(
      'handler' => 'views_handler_field',
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['panels_display']['storage_type'] = array(
    'title' => t('Storage type'),
    'help' => t('The Panels storage type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['panels_display']['storage_id'] = array(
    'title' => t('Storage ID'),
    'help' => t('The Panels storage ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  //
  // Panelizer integration.
  //

  if (module_exists('panelizer')) {
    // @todo Handle all Panelizer entity types
    $data['panels_display']['panelizer_node'] = array(
      'title' => t('Panelizer Node'),
      'help' => t('The Panelizer node this display exists on.'),
      'relationship' => array(
        'field' => 'panelizer_node',
        'handler' => 'views_panels_handler_relationship_panelizer_entity',
        'base' => 'node',
        'entity_type' => 'node',
        'relationship field' => 'did',
      ),
    );
  }

  return $data;
}

